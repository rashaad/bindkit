//
//  ViewController.swift
//  ObservableMVVM
//
//  Created by Rashaad Ramdeen on 10/3/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import UIKit
import BindKit

class ViewController: UIViewController {
    
    class ViewModel {
        var firstName = Observable("bob")
        var percentage = Observable(Float(0.5))
        var isToggled = Observable(false)
        var stepperValue = Observable(0.0)
        var buttonPressed = Observable(false)
        
        deinit {
            print("deinit: \(self)")
        }
    }
    
    var viewModel = ViewModel()
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var switchControl: UISwitch!
    @IBOutlet weak var stepperControl: UIStepper!
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var textFieldLabel: UILabel!
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var switchControlLabel: UILabel!
    @IBOutlet weak var stepperControlLabel: UILabel!
    @IBOutlet weak var buttonLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        try! textField.bind(viewModel.firstName)
        try! slider.bind(viewModel.percentage)
        try! switchControl.bind(viewModel.isToggled)
        try! stepperControl.bind(viewModel.stepperValue)
        try! button.bind(viewModel.buttonPressed)
        
        viewModel.firstName.bind { [unowned self] (observable, value) in
            self.textFieldLabel.text = "\(value)"
        }
        
        viewModel.percentage.bind { [unowned self] (observable, value) in
            self.sliderLabel.text = "\(value)"
        }
        
        viewModel.isToggled.bind { [unowned self] (observable, value) in
            self.switchControlLabel.text = value == true ? "true" : "false"
        }
        
        viewModel.stepperValue.bind { [unowned self] (observable, value) in
            self.stepperControlLabel.text = "\(value)"
        }
        
        viewModel.buttonPressed.bind { [unowned self] (observable, value) in
            self.buttonLabel.text = "\(value)"
        }
    }
    
    @IBAction func didPressNext() {
        navigationController?.setViewControllers([AnotherViewController()], animated: true)
    }
    
    deinit {
        print("deinit: \(self)")
    }
}

