//
//  AnotherViewController.swift
//  ObservableMVVM
//
//  Created by Rashaad Ramdeen on 10/12/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import UIKit

class AnotherViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Loaded AnotherViewController")
        view.backgroundColor = UIColor.green
    }
    
}
