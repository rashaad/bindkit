//
//  Observable.swift
//  ObservableMVVM
//
//  Created by Rashaad Ramdeen on 10/3/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import Foundation

public class Observable<DataType> {
    
    public typealias ObserverCallback = (_ observable: Observable<DataType>, DataType) -> Void
    
    private var observers = [ObserverCallback]()
    
    public var value: DataType {
        didSet {
            notifyObservers(value)
        }
    }
    
    public init(_ value: DataType) {
        self.value = value
    }
    
    public func bind(_ observer: @escaping ObserverCallback) {
        self.observers.append(observer)
        notifyObservers(value)
    }
    
    func notifyObservers(_ value: DataType) {
        observers.forEach { (observerCallback) in
            observerCallback(self, value)
        }
    }
    
    deinit {
        print("deinit: \(self)")
    }
}

