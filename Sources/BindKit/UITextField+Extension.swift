//
//  UITextField+Extension.swift
//  ObservableMVVM
//
//  Created by Rashaad Ramdeen on 10/12/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import UIKit

extension UITextField: Bindable {
    typealias DataType = String
    
    public func updateValue<DataType>(with value: DataType) {
        self.text = value as? String
    }
    
    override func valueChanged() {
        
        guard let text = text else {
            return
        }
        valueChanged(value: text)
    }
    
    public func bind(_ observable: Observable<String>) throws {
        setupEventListeners(events: [.editingChanged, .valueChanged])
        bindTo(observable)
    }
}
