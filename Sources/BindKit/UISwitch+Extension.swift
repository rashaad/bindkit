//
//  UISwitch+Extension.swift
//  MVVM
//
//  Created by Rashaad Ramdeen on 10/18/18.
//

import UIKit

extension UISwitch: Bindable {
    typealias DataType = Bool
    
    public func updateValue<DataType>(with value: DataType) {
        guard let value = value as? Bool else {
            return
        }
        self.isOn = value
    }
    
    override func valueChanged() {
        valueChanged(value: isOn)
    }
    
    public func bind(_ observable: Observable<Bool>) throws {
        setupEventListeners(events: [.valueChanged, .editingChanged])
        bindTo(observable)
    }
}

