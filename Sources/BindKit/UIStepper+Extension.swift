//
//  UIStepper+Extension.swift
//  MVVM
//
//  Created by Rashaad Ramdeen on 10/18/18.
//

import UIKit

extension UIStepper: Bindable {
    typealias DataType = Double
    
    public func updateValue<DataType>(with value: DataType) {
        guard let value = value as? Double else {
            return
        }
        self.value = value
    }
    
    override func valueChanged() {
        valueChanged(value: value)
    }
    
    public func bind(_ observable: Observable<Double>) throws {
        setupEventListeners(events: [.valueChanged, .editingChanged])
        bindTo(observable)
    }
}
