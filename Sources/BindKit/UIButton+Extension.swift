
//
//  UIButton+Extension.swift
//  MVVM
//
//  Created by Rashaad Ramdeen on 10/19/18.
//

import UIKit

extension UIButton: Bindable {
    typealias DataType = Bool
    
    public func updateValue<DataType>(with value: DataType) {
        return
    }
    
    override func valueChanged() {
        valueChanged(value: true)
    }
    
    public func bind(_ observable: Observable<Bool>) throws {
        setupEventListeners(events: [.touchUpInside])
        bindTo(observable)
    }
}
