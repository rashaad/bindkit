//
//  Bindable.swift
//  ObservableMVVM
//
//  Created by Rashaad Ramdeen on 10/12/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//


public protocol Bindable {
    func valueChanged<T>(value: T)
    func updateValue<T>(with value: T)
}

struct AssociatedKeys {
    static var observable: UInt8 = 0
}

public extension Bindable where Self: NSObject {
    
    func setObservable<DataType>(observable: Observable<DataType>) {
        objc_setAssociatedObject(self, &AssociatedKeys.observable, observable, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func getObservable<DataType>() -> Observable<DataType> {
        return objc_getAssociatedObject(self, &AssociatedKeys.observable) as! Observable<DataType>
    }
    
    internal func bindTo<DataType>(_ observable: Observable<DataType>) {
        setObservable(observable: observable)
        observable.bind { [weak self] (observable, value) in
            self?.updateValue(with: value)
        }
    }
    
    public func valueChanged<DataType>(value: DataType) {
        let observable = getObservable() as Observable<DataType>
        observable.value = value
    }
    
}
