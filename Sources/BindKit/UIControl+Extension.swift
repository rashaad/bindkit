//
//  UIControl+Extension.swift
//  ObservableMVVM
//
//  Created by Rashaad Ramdeen on 10/12/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import UIKit

extension UIControl {
    
    internal func setupEventListeners(events: UIControl.Event) {
        let actionsForSelf = actions(forTarget: self, forControlEvent: events) ?? [String]()
        
        guard actionsForSelf.isEmpty else {
            return
        }
        
        addTarget(self, action: #selector(valueChanged), for: events)
    }
    
    @objc func valueChanged() { /*override me */ }
}
