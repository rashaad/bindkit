//
//  UISlider+Extension.swift
//  MVVM
//
//  Created by Rashaad Ramdeen on 10/19/18.
//

import UIKit

extension UISlider: Bindable {
    typealias DataType = Float
    
    public func updateValue<DataType>(with value: DataType) {
        guard let floatValue = value as? Float else {
            return
        }
        
        self.value = floatValue
    }
    
    override func valueChanged() {
        valueChanged(value: value)
    }
    
    public func bind(_ observable: Observable<Float>) throws {
        setupEventListeners(events: [.valueChanged, .editingChanged])
        bindTo(observable)
    }
}
